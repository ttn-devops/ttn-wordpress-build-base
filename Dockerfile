FROM ubuntu:16.04

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y curl php-cli php-mbstring git unzip php7.0-curl && \
    curl -sL https://deb.nodesource.com/setup_4.x | bash - && \
    apt-get install -y nodejs && \
    npm install -g bower gulp-cli && \
    curl -sS https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    rm composer-setup.php && \
    composer global require "hirak/prestissimo:^0.3"
